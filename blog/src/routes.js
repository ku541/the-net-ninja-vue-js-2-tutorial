import ShowPost from './components/ShowPost.vue'
import CreatePost from './components/CreatePost.vue'
import IndexPosts from './components/IndexPosts.vue'

export default [
  { path: '/', component: IndexPosts },
  { path: '/create', component: CreatePost },
  { path: '/blog/:id', component: ShowPost }
]
