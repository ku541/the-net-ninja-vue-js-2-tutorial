import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: Routes
})

Vue.config.productionTip = false

// Directives
Vue.directive('theme', {
  bind: function (el, binding, vnode) {
    if (binding.value === 'wide') {
      el.style.maxWidth = '1200px'
    } else if (binding.value === 'narrow') {
      el.style.maxWidth = '560px'
    }

    if (binding.arg === 'column') {
      el.style.background = '#ddd'
      el.style.padding = '20px'
    }
  }
})

// Filters
Vue.filter('capitalize', function (value) {
  return value.toUpperCase()
})

new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')
